import Assembler from "./dist/src/Assembler.js"

const assembler = new Assembler()
const button = document.getElementById("button")
const sourceCode = document.getElementById("sourceCode")
const parseCode = document.getElementById("parseCode")
const byteCode = document.getElementById("byteCode")
const symbols = document.getElementById("symbols")

button.addEventListener("click", onClick)

function onClick() {
  parseCode.innerHTML = assembler.getInstructions(sourceCode.innerHTML).join("\n")
  byteCode.innerHTML = assembler.getByteCode(sourceCode.innerHTML)
}