import Parser from './Parser.js'
import Encoder from './Encoder.js'

export default class Assembler {
  private _encoder: Encoder
  private _parser: Parser

  constructor () {
    this._encoder = new Encoder()
    this._parser = new Parser()
  }

  // API
  // Pure
  public getByteCode (text: string): Uint8Array {
    const instructions = this._parser.parse(text)
    const hexDump = this._encoder.getHexDump(instructions)
    const byteCode = this._hextToBytes(hexDump)

    return byteCode
  }

  // Pure
  public getInstructions (text: string): string[] {
    const instructions = this._parser.parse(text)

    return instructions
  }

  // Methods
  // Pure
  private _hextToBytes (hexDump: string[]): Uint8Array {
    const len = hexDump.length
    const arrayBuffer = new ArrayBuffer(len)
    const result = new Uint8Array(arrayBuffer, 0, len)

    for (let i = 0; i < len; i++) {
      result[i] = Number('0x' + hexDump[i])
    }

    return result
  }
}
