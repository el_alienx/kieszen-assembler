import Definition from './encoder/Definition.js'
import ReplaceSymbol from './encoder/ReplaceSymbol.js'
import Label from './encoder/Label.js'
import AddressCode from './encoder/AddressCode.js'
import Value from './encoder/Value.js'
import ISymbol from './interfaces/ISymbol'
import OpcodeTable from '../node_modules/kieszen-core/dist/src/OpcodeTable.js'
import InstructionRegex from '../node_modules/kieszen-core/dist/src/InstructionRegex.js'
import ErrorEvent from '../node_modules/kieszen-core/dist/src/ErrorEvent.js'

export default class Encoder {
  // API
  // Impure, mutate received arg
  public getHexDump (lines: string[]): string[] {
    let hexDump: string[] = []
    const symbols: ISymbol[] = []

    // First pass
    // 1. Check line by line if it has a definition ('address' + 'name' + '@Number' ).
    symbols.push(...Definition.add(lines))

    // 2. If there are definitions in symbols, mutate the content of lines by replacing them with the content isnside the symbols array.
    if (symbols.length > 0) {
      lines = lines.map(line => ReplaceSymbol.definition(line, symbols))
    }

    // Second pass
    // 3. Check line by line if it has a label ('name' + ':') Note: Looks like we are storing a dummy value like the total code size in Label._extractValue().
    // 4. Encode the instructions
    for (const line of lines) {
      symbols.push(...Label.add(line, hexDump))
      hexDump.push(...this._encodeInstruction(line))
    }

    // 5. If there are definitions in symbols that ARE lables, replace the content of hexDump where the label values are, to the new relative positions.
    // 6. Check if there is unused labels pointing nowhere. In case somebody wants to BEQ to a non existing posisition.
    if (symbols.length > 0) {
      const labels = symbols.filter(item => item.type === 'label')
      hexDump = hexDump.map((item, index) => ReplaceSymbol.label(item, index, labels))
      this._checkUnusedLabels(hexDump)
    }

    return hexDump
  }

  // Methods
  // Impure, mutate received arg
  private _encodeInstruction (line: string): string[] {
    const isInstruction = InstructionRegex.opcode.test(line)
    if (!isInstruction) return []

    let value = Value.get(line) // goes first to prioritize his catch errors
    const mnemonic = line.slice(0, 3).toUpperCase()
    const addressCode = AddressCode.get(line)
    const opcode = OpcodeTable.getOpcode(mnemonic, addressCode)

    // Special case for JSR, JMP and branches
    if (addressCode === 2) value = Value.getLabelAbsolute(line)

    return [opcode, ...value]
  }

  private _checkUnusedLabels (hexDump: string[]): void {
    for (const index in hexDump) {
      const item = hexDump[index]
      const unusedLabel = item.match(InstructionRegex.keyword)
      const error = `The label ${item} does not go to any subroutine`

      if (unusedLabel !== null) throw new ErrorEvent(18, 'Assembler', item, error)
    }
  }
}
