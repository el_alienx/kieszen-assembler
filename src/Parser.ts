import ConvertNumber from '../node_modules/kieszen-core/dist/src/ConvertNumber.js'
export default class Parser {
  // API
  // Pure
  public parse (text: string): string[] {
    let result = this._textToArray(text)

    result = result.map(this._decimalToHex)
    result = result.map(this._removeComment)
    result = result.map(this._trimWhiteSpace)
    result = result.map(this._binaryToHex)
    result = result.map(this._lowerCaseString)
    result = result.filter(this._isLineEmpty)

    return result
  }

  // Methods
  // Pure
  private _binaryToHex (text: string): string {
    let binary = ''
    const regex = /%_?[0-1]{4}_?[0-1]{4}/ // includes Typescript undesrcore character to improve legibility
    const matchBinary = text.match(regex)

    binary = (matchBinary !== null) ? matchBinary[0] : ''
    if (binary.length === 0) return text

    binary = binary.replace('%', '')
    binary = binary.replace('_', '')
    const binaryToDecimal = parseInt(binary, 2)
    const decimalToHex = ConvertNumber.decimalToHex(binaryToDecimal)

    return text.replace(regex, '#' + decimalToHex)
  }

  // Pure
  private _decimalToHex (text: string): string {
    let decimal = ''

    // First pass
    // Regex floating here
    const firstRegex = /( |@)[\d]{1,}/i
    const matchFirst = text.match(firstRegex)

    decimal = (matchFirst !== null) ? matchFirst[0] : ''
    if (decimal.length === 0) return text

    // Second pass
    // Regex floating here
    const secondRegex = /[\d]{1,}/i
    const matchSecond = decimal.match(secondRegex)

    decimal = (matchSecond !== null) ? matchSecond[0] : ''
    if (decimal.length === 0) return decimal

    const decimalToHex = ConvertNumber.decimalToHex(Number(decimal))
    let result = ''

    // Third pass
    // If is inmediate, append #
    if (text.match(/@/)) {
      result = text.replace(decimal, decimalToHex)
    } else {
      result = text.replace(decimal, '#' + decimalToHex)
    }

    return result
  }

  // Pure
  private _isLineEmpty (text: string): boolean {
    return text.length > 0
  }

  // Pure
  private _lowerCaseString (text: string): string {
    return text.toLowerCase()
  }

  // Pure
  private _removeComment (text: string): string {
    const firstPass = text.replace(/\/\/.*/, '')
    const secondPass = firstPass.replace(/;.*/, '')

    return secondPass
  }

  // Pure
  private _textToArray (text: string): string[] {
    return text.split('\n')
  }

  // Pure
  private _trimWhiteSpace (text: string): string {
    return text.trim()
  }
}
