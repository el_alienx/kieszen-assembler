// eslint-disable-next-line no-unused-vars
import IAddressCode from '../interfaces/IAddressCode.js'
import ErrorEvent from '../../node_modules/kieszen-core/dist/src/ErrorEvent.js'

export default abstract class AddressCode {
  // # API
  // Pure
  public static get (instruction: string): number {
    let result: number = -1
    const addressCodes = this._getAddressCodes()
    const error = `The instruction ${instruction} did not match any know pattern`

    addressCodes.forEach((item, index) => {
      const addressCode = item.regex

      if (addressCode.test(instruction)) result = index
    })

    if (result === -1) throw new ErrorEvent(1, 'Assembler', instruction, error)
    return result
  }

  // # Methods
  // Pure
  private static _getAddressCodes (): IAddressCode[] {
    return [
      { name: 'Implied, Acummulator, Stack', regex: /^[A-Z]{3}$/i },
      { name: 'Inmediate', regex: /\#[A-F\d]{1,2}$/i },
      { name: 'Relative, Absolute with label', regex: /[^@]\w{4,}/i },
      { name: 'ZeroPage', regex: /\@[A-F\d]{1,2}$/i },
      { name: 'ZeroPageX', regex: /\@\w{1,2}\s?,\s?X$/i },
      { name: 'ZeroPageY', regex: /\@\w{1,2}\s?,\s?Y$/i },
      { name: 'IndirectX', regex: /\(\s?\@[A-F\d]{1,2}\s?,\s?X\s?\)/i },
      { name: 'IndirectY', regex: /\(\s?\@[A-F\d]{1,2}\s?\)\s?,\s?Y/i },
      { name: 'Absolute', regex: /\@[A-F\d]{4}$/i },
      { name: 'Absolute indirect', regex: /\(\s?\@[A-F\d]{4}\s?\)/i },
      { name: 'AbsoluteX', regex: /\@[A-F\d]{4}\s?,\s?X/i },
      { name: 'AbsoluteY', regex: /\@[A-F\d]{4}\s?,\s?Y/i }
    ]
  }
}
