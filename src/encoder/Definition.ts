// eslint-disable-next-line no-unused-vars
import ISymbol from '../interfaces/ISymbol'
import Value from './Value.js'
import ErrorEvent from '../../node_modules/kieszen-core/dist/src/ErrorEvent.js'
import InstructionRegex from '../../node_modules/kieszen-core/dist/src/InstructionRegex.js'

export default abstract class Definition {
  // API
  // Pure
  public static add (lines: string[]): ISymbol[] {
    const result: ISymbol[] = []

    for (const line of lines) {
      const isDefinition = InstructionRegex.definition.test(line)
      if (!isDefinition) continue

      const name = this._extractName(line)
      const value = this._extractValue(line)

      if (this._isDuplicate(result, name)) continue
      result.push({ name: name, type: 'definition', value: value })
    }

    return result
  }

  // Methods
  // Pure
  private static _extractName (line: string): string {
    const regexes = line.match(InstructionRegex.keyword) || []
    const keywords = line.split(' ').length
    const result = (regexes !== null) ? regexes[1] : undefined
    const error1 = `The definition ${regexes} contains more than 1 word`
    const error2 = `The definition name ${line} is to short`

    if (keywords > 3) throw new ErrorEvent(15, 'Assembler', line, error1)
    if (result === undefined) throw new ErrorEvent(2, 'Assembler', line, error2)
    // @ts-ignore
    return result
  }

  // Pure
  private static _extractValue (line: string): string {
    const regexes = line.match(InstructionRegex.value) || []
    const result = (regexes !== null) ? regexes[0] : undefined
    const error1 = `The definition ${line} does not have any asigned value`
    const error2 = `The definition ${line} is using an immediate value`

    if (result === undefined) throw new ErrorEvent(20, 'Assembler', line, error1)
    // @ts-ignore
    if (result.charAt(0) === '#') throw new ErrorEvent(19, 'Assembler', line, error2)
    return Value.get(line).reverse().join('')
  }

  // Pure
  private static _isDuplicate (list: ISymbol[], name: string): Boolean {
    const error = `The definition name ${name} already exist`
    let result = false

    for (const item of list) {
      if (item.name === name) {
        result = true
        break
      }
    }

    if (result) throw new ErrorEvent(14, 'Assembler', name, error)
    return result
  }
}
