// eslint-disable-next-line no-unused-vars
import ISymbol from '../interfaces/ISymbol'
import ConvertNumber from '../../node_modules/kieszen-core/dist/src/ConvertNumber.js'
import ErrorEvent from '../../node_modules/kieszen-core/dist/src/ErrorEvent.js'
import InstructionRegex from '../../node_modules/kieszen-core/dist/src/InstructionRegex.js'
import MemoryMap from '../../node_modules/kieszen-core/dist/src/MemoryMap.js'

export default abstract class Label {
  // API
  // Pure
  public static add (line: string, hexDump: string[]): ISymbol[] {
    const isLabel = InstructionRegex.label.test(line)
    if (!isLabel) return []

    const name = this._extractName(line)
    const value = this._extractValue(hexDump)
    const result = [{ name: name, type: 'label', value: value }]

    return result
  }

  // Methods
  // Pure
  private static _extractName (line: string): string {
    const regexes = line.match(InstructionRegex.keyword)
    const result = (regexes !== null) ? regexes[0] : undefined
    const error = `The label name ${line} is invalid`

    if (result === undefined) throw new ErrorEvent(3, 'Assembler', line, error)
    // @ts-ignore
    return result
  }

  // Pure
  private static _extractValue (hexArray: string[]): string {
    const position = hexArray.length + MemoryMap.programCode.start

    return ConvertNumber.decimalToHex(position)
  }
}
