// eslint-disable-next-line no-unused-vars
import ISymbol from '../interfaces/ISymbol'
import ConvertNumber from '../../node_modules/kieszen-core/dist/src/ConvertNumber.js'
import InstructionRegex from '../../node_modules/kieszen-core/dist/src/InstructionRegex.js'

export default class ReplaceSymbol {
  // # API
  // Pure
  public static definition (line: string, symbols: ISymbol[]): string {
    const isDefinition = InstructionRegex.definition.test(line)
    if (isDefinition) return line

    for (const symbol of symbols) {
      const name = new RegExp('\\b' + symbol.name + '\\b')
      const value = '@' + symbol.value

      line = line.replace(name, value)
    }

    return line
  }

  // Pure
  public static label (item: string, index: number, symbols: ISymbol[]): string {
    const isLabel = item.length > 2
    if (!isLabel) return item

    for (const symbol of symbols) {
      const name = symbol.name
      const value = symbol.value

      if (item === name + 'LO') item = ConvertNumber.lowByte(value)
      if (item === name + 'HI') item = ConvertNumber.highByte(value)
    }

    return item
  }
}
