
import ConvertNumber from '../../node_modules/kieszen-core/dist/src/ConvertNumber.js'
import ErrorEvent from '../../node_modules/kieszen-core/dist/src/ErrorEvent.js'
import InstructionRegex from '../../node_modules/kieszen-core/dist/src/InstructionRegex.js'

export default abstract class Value {
  // API
  // Pure
  public static get (line: string): string[] {
    let result: string[] = []
    const rawValue = InstructionRegex.value.exec(line)
    const parsedValue = (rawValue !== null) ? rawValue[0].slice(1) : ''
    const len = parsedValue.length
    const error = `The number in ${line} is to long`

    if (len === 4) result = this._stringToLittleEndian(parsedValue)
    if (len === 3) result = this._stringToLittleEndian(parsedValue.padStart(4, '0'))
    if (len === 2) result = [parsedValue]
    if (len === 1) result = [parsedValue.padStart(2, '0')]

    if (len > 4) throw new ErrorEvent(5, 'Assembler', line, error)
    return result
  }

  // Pure
  public static getLabelAbsolute (line: string): string[] {
    // Warning do not replace by global Regex intruction until check that it works
    const rawValue = /\w{4,}/gi.exec(line)
    const error = `The jump ${line} has an invalid absolute address mode value`
    if (rawValue === null) throw new ErrorEvent(6, 'Assembler', line, error)

    // @ts-ignore
    const labelLowByte = rawValue[0] + 'LO'
    // @ts-ignore
    const labelHighByte = rawValue[0] + 'HI'

    return [labelLowByte, labelHighByte]
  }

  // Methods
  // Pure
  private static _stringToLittleEndian (value: string): string[] {
    const lowByte = ConvertNumber.lowByte(value)
    const highByte = ConvertNumber.highByte(value)

    return [lowByte, highByte]
  }
}
