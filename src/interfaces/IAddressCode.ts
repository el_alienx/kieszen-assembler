export default interface IAddressCode {
  name: string
  regex: RegExp
}
