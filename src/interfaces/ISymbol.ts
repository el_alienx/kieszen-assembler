export default interface ISymbol {
  name: string
  type: string
  value: string
}
